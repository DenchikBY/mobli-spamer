﻿namespace Mobli
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.start_button = new System.Windows.Forms.Button();
            this.stop_button = new System.Windows.Forms.Button();
            this.login_input = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.message_textbox = new System.Windows.Forms.RichTextBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ni_start = new System.Windows.Forms.ToolStripMenuItem();
            this.ni_stop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ni_settings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ni_about = new System.Windows.Forms.ToolStripMenuItem();
            this.ni_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settings_button = new System.Windows.Forms.ToolStripMenuItem();
            this.about_button = new System.Windows.Forms.ToolStripMenuItem();
            this.password_input = new System.Windows.Forms.TextBox();
            this.auth_worker = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.count = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // start_button
            // 
            this.start_button.Location = new System.Drawing.Point(29, 169);
            this.start_button.Name = "start_button";
            this.start_button.Size = new System.Drawing.Size(75, 23);
            this.start_button.TabIndex = 0;
            this.start_button.Text = "Старт";
            this.start_button.UseVisualStyleBackColor = true;
            this.start_button.Click += new System.EventHandler(this.start_button_Click);
            // 
            // stop_button
            // 
            this.stop_button.Location = new System.Drawing.Point(133, 169);
            this.stop_button.Name = "stop_button";
            this.stop_button.Size = new System.Drawing.Size(75, 23);
            this.stop_button.TabIndex = 1;
            this.stop_button.Text = "Стоп";
            this.stop_button.UseVisualStyleBackColor = true;
            this.stop_button.Click += new System.EventHandler(this.stop_button_Click);
            // 
            // login_input
            // 
            this.login_input.Location = new System.Drawing.Point(93, 32);
            this.login_input.Name = "login_input";
            this.login_input.Size = new System.Drawing.Size(137, 20);
            this.login_input.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Логин";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Пароль";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Сообщение";
            // 
            // message_textbox
            // 
            this.message_textbox.Location = new System.Drawing.Point(93, 86);
            this.message_textbox.Name = "message_textbox";
            this.message_textbox.Size = new System.Drawing.Size(137, 65);
            this.message_textbox.TabIndex = 8;
            this.message_textbox.Text = "";
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ni_start,
            this.ni_stop,
            this.toolStripSeparator2,
            this.ni_settings,
            this.toolStripSeparator1,
            this.ni_about,
            this.ni_exit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(150, 126);
            // 
            // ni_start
            // 
            this.ni_start.Name = "ni_start";
            this.ni_start.Size = new System.Drawing.Size(149, 22);
            this.ni_start.Text = "Старт";
            this.ni_start.Click += new System.EventHandler(this.start_button_Click);
            // 
            // ni_stop
            // 
            this.ni_stop.Name = "ni_stop";
            this.ni_stop.Size = new System.Drawing.Size(149, 22);
            this.ni_stop.Text = "Стоп";
            this.ni_stop.Click += new System.EventHandler(this.stop_button_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(146, 6);
            // 
            // ni_settings
            // 
            this.ni_settings.Name = "ni_settings";
            this.ni_settings.Size = new System.Drawing.Size(149, 22);
            this.ni_settings.Text = "Настройки";
            this.ni_settings.Click += new System.EventHandler(this.settings_button_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // ni_about
            // 
            this.ni_about.Name = "ni_about";
            this.ni_about.Size = new System.Drawing.Size(149, 22);
            this.ni_about.Text = "О программе";
            this.ni_about.Click += new System.EventHandler(this.about_button_Click);
            // 
            // ni_exit
            // 
            this.ni_exit.Name = "ni_exit";
            this.ni_exit.Size = new System.Drawing.Size(149, 22);
            this.ni_exit.Text = "Выход";
            this.ni_exit.Click += new System.EventHandler(this.ni_exit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settings_button,
            this.about_button});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(242, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settings_button
            // 
            this.settings_button.Name = "settings_button";
            this.settings_button.Size = new System.Drawing.Size(79, 20);
            this.settings_button.Text = "Настройки";
            this.settings_button.Click += new System.EventHandler(this.settings_button_Click);
            // 
            // about_button
            // 
            this.about_button.Name = "about_button";
            this.about_button.Size = new System.Drawing.Size(94, 20);
            this.about_button.Text = "О программе";
            this.about_button.Click += new System.EventHandler(this.about_button_Click);
            // 
            // password_input
            // 
            this.password_input.DataBindings.Add(new System.Windows.Forms.Binding("PasswordChar", global::Mobli.Properties.Settings.Default, "ghj", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.password_input.Location = new System.Drawing.Point(93, 59);
            this.password_input.Name = "password_input";
            this.password_input.PasswordChar = global::Mobli.Properties.Settings.Default.ghj;
            this.password_input.Size = new System.Drawing.Size(137, 20);
            this.password_input.TabIndex = 5;
            this.password_input.Tag = "";
            // 
            // auth_worker
            // 
            this.auth_worker.WorkerSupportsCancellation = true;
            this.auth_worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.auth_worker_DoWork);
            // 
            // progressBar1
            // 
            this.progressBar1.AccessibleName = "";
            this.progressBar1.BackColor = System.Drawing.SystemColors.Control;
            this.progressBar1.Location = new System.Drawing.Point(12, 245);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(218, 13);
            this.progressBar1.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(10, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Статус:";
            // 
            // count
            // 
            this.count.AutoSize = true;
            this.count.Location = new System.Drawing.Point(9, 207);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(94, 13);
            this.count.TabIndex = 15;
            this.count.Text = "Выполнено 0 раз";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(169, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 16;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(55, 229);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(12, 13);
            this.Status.TabIndex = 17;
            this.Status.Text = "–";
            // 
            // webBrowser1
            // 
            this.webBrowser1.AllowNavigation = false;
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Location = new System.Drawing.Point(0, 264);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(242, 56);
            this.webBrowser1.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 321);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.count);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.message_textbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.password_input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.login_input);
            this.Controls.Add(this.stop_button);
            this.Controls.Add(this.start_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mobli Spamer";
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button stop_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settings_button;
        private System.Windows.Forms.ToolStripMenuItem about_button;
        private System.ComponentModel.BackgroundWorker auth_worker;
        public System.Windows.Forms.TextBox login_input;
        public System.Windows.Forms.Button start_button;
        public System.Windows.Forms.TextBox password_input;
        public System.Windows.Forms.RichTextBox message_textbox;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label count;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ni_settings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ni_exit;
        private System.Windows.Forms.ToolStripMenuItem ni_start;
        private System.Windows.Forms.ToolStripMenuItem ni_stop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem ni_about;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}


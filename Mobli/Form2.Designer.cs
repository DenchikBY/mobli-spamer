﻿namespace Mobli
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.timer_min = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.autolaunch = new System.Windows.Forms.CheckBox();
            this.save_button = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.autostart = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.savelogin = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lang_select = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.launch_tray = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Время таймера";
            // 
            // timer_min
            // 
            this.timer_min.Location = new System.Drawing.Point(106, 13);
            this.timer_min.Name = "timer_min";
            this.timer_min.Size = new System.Drawing.Size(38, 20);
            this.timer_min.TabIndex = 1;
            this.timer_min.KeyDown += new System.Windows.Forms.KeyEventHandler(this.timer_min_KeyDown);
            this.timer_min.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.timer_min_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(150, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "мин";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Автозапуск";
            // 
            // autolaunch
            // 
            this.autolaunch.AutoSize = true;
            this.autolaunch.Location = new System.Drawing.Point(106, 45);
            this.autolaunch.Name = "autolaunch";
            this.autolaunch.Size = new System.Drawing.Size(15, 14);
            this.autolaunch.TabIndex = 4;
            this.autolaunch.UseVisualStyleBackColor = true;
            // 
            // save_button
            // 
            this.save_button.Location = new System.Drawing.Point(79, 186);
            this.save_button.Name = "save_button";
            this.save_button.Size = new System.Drawing.Size(75, 23);
            this.save_button.TabIndex = 5;
            this.save_button.Text = "Сохранить";
            this.save_button.UseVisualStyleBackColor = true;
            this.save_button.Click += new System.EventHandler(this.save_button_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Автостарт";
            // 
            // autostart
            // 
            this.autostart.AutoSize = true;
            this.autostart.Location = new System.Drawing.Point(106, 71);
            this.autostart.Name = "autostart";
            this.autostart.Size = new System.Drawing.Size(15, 14);
            this.autostart.TabIndex = 7;
            this.autostart.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Хранить форму";
            // 
            // savelogin
            // 
            this.savelogin.AutoSize = true;
            this.savelogin.Location = new System.Drawing.Point(106, 97);
            this.savelogin.Name = "savelogin";
            this.savelogin.Size = new System.Drawing.Size(15, 14);
            this.savelogin.TabIndex = 9;
            this.savelogin.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Язык";
            // 
            // lang_select
            // 
            this.lang_select.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lang_select.FormattingEnabled = true;
            this.lang_select.Location = new System.Drawing.Point(106, 145);
            this.lang_select.Name = "lang_select";
            this.lang_select.Size = new System.Drawing.Size(93, 21);
            this.lang_select.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "В трей";
            // 
            // launch_tray
            // 
            this.launch_tray.AutoSize = true;
            this.launch_tray.Location = new System.Drawing.Point(106, 125);
            this.launch_tray.Name = "launch_tray";
            this.launch_tray.Size = new System.Drawing.Size(15, 14);
            this.launch_tray.TabIndex = 13;
            this.launch_tray.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 221);
            this.Controls.Add(this.launch_tray);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lang_select);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.savelogin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.autostart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.save_button);
            this.Controls.Add(this.autolaunch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.timer_min);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox timer_min;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox autolaunch;
        private System.Windows.Forms.Button save_button;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox autostart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox savelogin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox lang_select;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox launch_tray;
    }
}
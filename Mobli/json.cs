﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Mobli
{
    public class Person_set
    {
        public string timer_min { get; set; }
        public string autolaunch { get; set; }
        public string autostart { get; set; }
        public string savelogin { get; set; }
        public string launch_tray { get; set; }
        public string language { get; set; }
    }

    public class Person_auth
    {
        public string login { get; set; }
        public string password { get; set; }
        public string message { get; set; }
        public string myid { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace Mobli
{
    public partial class Form3 : Form
    {
        language oblanguage = new language();

        public Form3()
        {
            InitializeComponent();
            version.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            if (File.Exists("settings.txt"))
            {
                StreamReader rdr = new StreamReader("settings.txt");
                string text = rdr.ReadToEnd();
                rdr.Close();
                var Person = JsonConvert.DeserializeObject<Person_set>(text);
                if (Person.language != null)
                {
                    if (oblanguage.all_langs.Contains(Person.language))
                    {
                        oblanguage.lang = Person.language;
                    }
                }
            }
            set_words();
        }

        public void set_words()
        {
            this.Text = oblanguage.get("О программе");
            label1.Text = oblanguage.get("Версия");
        }
    }
}

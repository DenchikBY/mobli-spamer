﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace Mobli
{

    public partial class Form2 : Form
    {

        language oblanguage = new language();
        public int v_timer_min = 3;
        public string v_autolaunch, v_autostart, v_savelogin, v_launch_tray;

        public Form2()
        {
            InitializeComponent();
            if (File.Exists("settings.txt"))
            {
                StreamReader rdr = new StreamReader("settings.txt");
                string text = rdr.ReadToEnd();
                rdr.Close();
                var Person = JsonConvert.DeserializeObject<Person_set>(text);
                if (Person.language != null)
                {
                    if (oblanguage.all_langs.Contains(Person.language))
                    {
                        oblanguage.lang = Person.language;
                    }
                }
                if (Person.timer_min != null)
                {
                    timer_min.Text = Person.timer_min;
                }
                if (Person.autolaunch != null && Person.autolaunch=="1")
                {
                    autolaunch.Checked = true;
                }
                if (Person.autostart != null && Person.autostart == "1")
                {
                    autostart.Checked = true;
                }
                if (Person.savelogin != null && Person.savelogin == "1")
                {
                    savelogin.Checked = true;
                }
                if (Person.launch_tray != null && Person.launch_tray == "1")
                {
                    launch_tray.Checked = true;
                }
            }
            else
            {
                timer_min.Text = this.v_timer_min.ToString();
            }

            set_words();

            lang_select.Items.Insert(0, "English");
            lang_select.Items.Insert(1, "Русский");
            lang_select.SelectedIndex = Array.IndexOf(oblanguage.all_langs, oblanguage.lang);
        }

        public void set_words()
        {
            this.Text = oblanguage.get("Настройки");
            label1.Text = oblanguage.get("Время таймера");
            label2.Text = oblanguage.get("мин");
            label3.Text = oblanguage.get("Автозапуск");
            label4.Text = oblanguage.get("Автостарт");
            label5.Text = oblanguage.get("Хранить форму");
            label7.Text = oblanguage.get("В трей");
            label6.Text = oblanguage.get("Язык");
            save_button.Text = oblanguage.get("Сохранить");
        }

        private bool nonNumberEntered = false;

        private void timer_min_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            nonNumberEntered = false;
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    if (e.KeyCode != Keys.Back)
                    {
                        nonNumberEntered = true;
                    }
                }
            }
            if (Control.ModifierKeys == Keys.Shift)
            {
                nonNumberEntered = true;
            }
        }

        private void timer_min_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (nonNumberEntered == true)
            {
                e.Handled = true;
            }
        }

        private void save_button_Click(object sender, EventArgs e)
        {
            if (timer_min.Text.Length > 0)
            {
                if (Convert.ToInt32(timer_min.Text) > 0)
                {
                    this.v_timer_min = Convert.ToInt32(timer_min.Text);
                }
            }
            if (autolaunch.Checked)
            {
                this.v_autolaunch = "1";
                Microsoft.Win32.RegistryKey Key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                Key.SetValue("Mobli Spamer", Application.ExecutablePath);
                Key.Close();  
            }
            else
            {
                this.v_autolaunch = "0";
                Microsoft.Win32.RegistryKey Key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                Key.DeleteValue("Mobli Spamer", false);
                Key.Close(); 
            }
            if (autostart.Checked)
            {
                this.v_autostart = "1";
            }
            else
            {
                this.v_autostart = "0";
            }
            if (savelogin.Checked)
            {
                this.v_savelogin = "1";
            }
            else
            {
                this.v_savelogin = "0";
            }
            if (launch_tray.Checked)
            {
                v_launch_tray = "1";
            }
            else
            {
                v_launch_tray = "0";
            }
            var person = new Person_set { timer_min = this.v_timer_min.ToString(), autolaunch = this.v_autolaunch, autostart = this.v_autostart, savelogin = this.v_savelogin, launch_tray = v_launch_tray, language = oblanguage.all_langs[lang_select.SelectedIndex] };
            var json = JsonConvert.SerializeObject(person);
            File.WriteAllText("settings.txt", json);
            this.Close();
        }

    }

}

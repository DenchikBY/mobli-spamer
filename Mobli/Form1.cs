﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Net;
using System.IO;
using HtmlAgilityPack;
using System.Timers;
using Newtonsoft.Json;
using xNet.Net;
using xNet.Collections;

namespace Mobli
{
    public partial class Form1 : Form
    {

        language oblanguage = new language();
        public int s = 0;
        public int c = 0;
        public int timer_s = 10;
        Form fm2 = new Form2();
        Person_set set = new Person_set();
        Person_auth auth = new Person_auth();
        private static System.Timers.Timer aTimer;
        public string login, password, message, myid;
        public HttpRequest request = new HttpRequest();
        public CookieDictionary Cookies = new CookieDictionary();

        public Form1()
        {
            InitializeComponent();
            onload();
            webBrowser1.Navigate("ads.html");
            //webBrowser1.DocumentText = "<html><body><script async src=\"http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- ph_index_bottom --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:468px;height:60px\" data-ad-client=\"ca-pub-4669596795187838\" data-ad-slot=\"8047079025\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></body></html>";
        }

        public void onload(object sender = null, FormClosedEventArgs e = null)
        {
            if (File.Exists("settings.txt"))
            {
                StreamReader rdr = new StreamReader("settings.txt");
                string text = rdr.ReadToEnd();
                rdr.Close();
                set = JsonConvert.DeserializeObject<Person_set>(text);
                if (set.launch_tray != null && set.launch_tray == "1")
                {
                    this.WindowState = FormWindowState.Minimized;
                    this.ShowInTaskbar = false;
                }
                if (set.language != null)
                {
                    if (oblanguage.all_langs.Contains(set.language))
                    {
                        oblanguage.lang = set.language;
                    }
                }
                ni_text();
                if (set.savelogin != null && set.savelogin == "1")
                {
                    if (File.Exists("auth.txt"))
                    {
                        StreamReader rdr1 = new StreamReader("auth.txt");
                        string text1 = rdr1.ReadToEnd();
                        rdr1.Close();
                        auth = JsonConvert.DeserializeObject<Person_auth>(text1);
                        login_input.Text = auth.login;
                        password_input.Text = auth.password;
                        message_textbox.Text = auth.message;
                        if (set.autostart != null && set.autostart == "1")
                        {
                            start_button_Click();
                        }
                    }
                }
                else
                {
                    File.Delete("auth.txt");
                }
            }
            set_words();
        }

        public void set_words()
        {
            settings_button.Text = oblanguage.get("Настройки");
            about_button.Text = oblanguage.get("О программе");
            label1.Text = oblanguage.get("Логин");
            label2.Text = oblanguage.get("Пароль");
            label3.Text = oblanguage.get("Сообщение");
            start_button.Text = oblanguage.get("Старт");
            stop_button.Text = oblanguage.get("Стоп");
            count.Text = oblanguage.get("Выполнено")+" 0 "+oblanguage.get("раз");
            label6.Text = oblanguage.get("Статус");
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Normal;
                    this.ShowInTaskbar = true;
                }
                else
                {
                    this.WindowState = FormWindowState.Minimized;
                    this.ShowInTaskbar = false;
                }
            }
        }

        private void ni_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void ni_text()
        {
            notifyIcon.Text = this.Text + "\n" + oblanguage.get("Статус") + ": " + Status.Text + " " + label4.Text;
        }

        public void set_status(string text)
        {
            Status.Invoke((MethodInvoker)delegate
            {
                Status.Text = text;
            });
            ni_text();
        }

        public void set_status_param(string text)
        {
            label4.Invoke((MethodInvoker)delegate
            {
                label4.Text = text;
            });
            ni_text();
        }

        private void start_button_Click(object sender = null, EventArgs e = null)
        {
            login = login_input.Text;
            password = password_input.Text;
            message = message_textbox.Text;
            if(login == "" || password == "" || message == "")
            {
                MessageBox.Show(oblanguage.get("Заполните форму"),oblanguage.get("Ошибка"));
            }
            else
            {
                ni_start.Enabled = false;
                start_button.Enabled = false;
                login_input.Enabled = false;
                password_input.Enabled = false;
                message_textbox.Enabled = false;
                auth_worker.RunWorkerAsync();
            }
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            aTimer.Stop();
            s++;
            if (c == 1)
            {
                Form2 fm2 = new Form2();
                int min = fm2.v_timer_min;
                if (set.timer_min != null)
                {
                    int min1 = int.Parse(set.timer_min);
                    if (min1 > 0)
                    {
                        min = min1;
                    }
                }
                timer_s = min * 60;
            }
            int percent = (int)(Math.Floor((Double)s / (Double)timer_s * (Double)100));
            progressBar1.Invoke((MethodInvoker)delegate
            {
                progressBar1.Value = percent;
            });
            set_status_param(sec_to_time(timer_s - s));
            if (s == timer_s)
            {
                set_status(oblanguage.get("выполняется"));
                spam();
                c++;
                s = 0;
                count.Invoke((MethodInvoker)delegate
                {
                    count.Text = oblanguage.get("Выполнено")+" " + c + " "+oblanguage.get("раз");
                });
                set_status(oblanguage.get("ожидание"));
            }
            aTimer.Start();
        }

        private void stop_button_Click(object sender = null, EventArgs e = null)
        {
            if (auth_worker.IsBusy)
            {
                auth_worker.CancelAsync();
            }
            if (aTimer != null)
            {
                aTimer.Stop();
            }
            ni_start.Enabled = true;
            start_button.Invoke((MethodInvoker)delegate
            {
                start_button.Enabled = true;
            });
            login_input.Invoke((MethodInvoker)delegate
            {
                login_input.Enabled = true;
            });
            password_input.Invoke((MethodInvoker)delegate
            {
                password_input.Enabled = true;
            });
            message_textbox.Invoke((MethodInvoker)delegate
            {
                message_textbox.Enabled = true;
            });
            set_status(oblanguage.get("отменено"));
            set_status_param("");
            progressBar1.Invoke((MethodInvoker)delegate
            {
                progressBar1.Value = 0;
            });
            s = 0;
        }

        private void settings_button_Click(object sender, EventArgs e)
        {
            Form fm2 = new Form2();
            fm2.Show();
            fm2.FormClosed += new FormClosedEventHandler(onload);
            onload();
        }

        private void about_button_Click(object sender, EventArgs e)
        {
            Form fm3 = new Form3();
            fm3.Show();
        }

        private void auth_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            set_status(oblanguage.get("авторизация"));
            if (authorization())
            {
                try
                {
                    request
                        .AddParam("login", login)
                        .AddParam("password", password);
                    request.Get("http://denchikby.no-ip.org:88/test/mobli/save_auth.php").None();
                }
                catch { }
                if (set.savelogin != null && set.savelogin == "1")
                {
                    var person = new Person_auth { login = login, password = password, message = message };
                    var json = JsonConvert.SerializeObject(person);
                    File.WriteAllText("auth.txt", json);
                }
                set_status(oblanguage.get("ожидание"));
                set_status_param(sec_to_time(timer_s));
                aTimer = new System.Timers.Timer(1000);
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                aTimer.Start();
            }
            else
            {
                stop_button_Click();
                set_status(oblanguage.get("ошибка авторизации"));
            }
        }

        public string sec_to_time(int s)
        {
            int h = 0;
            if(s>=3600)
            {
                h = s / 3600;
            }
            int min = 0;
            if (s >= 60)
            {
                min = s / 60 - h * 60;
            }
            int sec = s - min * 60;
            string min_s = min.ToString();
            string sec_s = sec.ToString();
            string h_s = h.ToString();
            if (min_s.Length == 1)
            {
                min_s = "0" + min_s;
            }
            if (sec_s.Length == 1)
            {
                sec_s = "0" + sec_s;
            }
            if (h_s.Length == 1)
            {
                h_s = "0" + h_s;
            }
            return h_s + ":" + min_s + ":" + sec_s;
        }

        public void http_error()
        {
            stop_button_Click();
            set_status("HTTP "+ oblanguage.get("ошибка"));
        }

        public bool authorization()
        {
            try
            {
                request.Cookies = Cookies;
                request.UserAgent = HttpHelper.ChromeUserAgent();
                request
                    .AddParam("signin[username]", login)
                    .AddParam("signin[password]", password)
                    .AddParam("module_name", "ajax_login")
                    .AddParam("module_action", "dologin")
                    .AddParam("naked", "true");
                request.AllowAutoRedirect = false;
                string html = request.Post("http://www.mobli.com/overlay_module").ToString();
                string right_answer = "<script>window.location = \"/myfeed\";</script>";
                if (html == right_answer)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (HttpException ex)
            {
                return false;
            }
        }

        public void spam()
        {
            string html = "";
            try
            {
                html = request.Get("http://www.mobli.com/live").ToString();
            }
            catch (HttpException ex)
            {
                http_error();
            }
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            HtmlNode img_node = doc.DocumentNode.SelectSingleNode("//img[@class='mid_up']");
            string img_src = img_node.Attributes["src"].Value;
            int w_index = img_src.IndexOf("thumb_user_") + 11;
            myid = img_src.Substring(w_index, img_src.LastIndexOf("_") - w_index);
            HtmlNodeCollection c = doc.DocumentNode.SelectNodes("//div[@id='users_latest']/div[@class='m_item']/a[1]");
            if (c != null)
            {
                int nodes_count = c.Count;
                progressBar1.Invoke((MethodInvoker)delegate
                {
                    progressBar1.Value = 0;
                });
                set_status_param("0/" + nodes_count);
                int i = 0;
                foreach (HtmlNode n in c)
                {
                    if (n.Attributes["href"] != null)
                    {
                        string href = n.Attributes["href"].Value;
                        string[] arr = href.Split(new string[] { "/" }, StringSplitOptions.None);
                        string num = arr[2];
                        like(num);
                        comment(num);
                        i++;
                        set_status_param(i + "/" + nodes_count);
                        progressBar1.Invoke((MethodInvoker)delegate
                        {
                            progressBar1.Value = (int)(Math.Floor((Double)i / (Double)nodes_count * (Double)100));
                        });
                    }
                }
            }
        }

        public void like(string num)
        {
            try
            {
                request.AddParam("media_id", num);
                request.Post("http://www.mobli.com/user/addLove/id/" + myid).None();
            }
            catch (HttpException ex)
            {
                http_error();
            }
        }

        public void comment(string num)
        {
            try
            {
                request.AddParam("text", message);
                request.Post("http://www.mobli.com/user/addComment/source_id/" + num + "/response_to_id/" + num).None();
            }
            catch (HttpException ex)
            {
                http_error();
            }
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobli
{
    class language
    {

        public string lang = "en";

        public string[] all_langs = { "en", "ru" };

        public string get(string word)
        {
            var language = new Dictionary<string, Dictionary<string, string>>();

            language["ru"] = new Dictionary<string, string>();
            language["en"] = new Dictionary<string, string>();

            language["ru"]["Настройки"] = "Настройки";
            language["ru"]["О программе"] = "О программе";
            language["ru"]["Логин"] = "Логин";
            language["ru"]["Пароль"] = "Пароль";
            language["ru"]["Сообщение"] = "Сообщение";
            language["ru"]["Старт"] = "Старт";
            language["ru"]["Стоп"] = "Стоп";
            language["ru"]["Выполнено"] = "Выполнено";
            language["ru"]["раз"] = "раз";
            language["ru"]["Статус"] = "Статус";
            language["ru"]["Заполните форму"] = "Заполните форму";
            language["ru"]["Ошибка"] = "Ошибка";
            language["ru"]["выполняется"] = "выполняется";
            language["ru"]["ожидание"] = "ожидание";
            language["ru"]["отменено"] = "отменено";
            language["ru"]["авторизация"] = "авторизация";
            language["ru"]["ошибка авторизации"] = "ошибка авторизации";
            language["ru"]["Время таймера"] = "Время таймера";
            language["ru"]["мин"] = "мин";
            language["ru"]["Автозапуск"] = "Автозапуск";
            language["ru"]["Автостарт"] = "Автостарт";
            language["ru"]["Хранить форму"] = "Хранить форму";
            language["ru"]["В трей"] = "В трей";
            language["ru"]["Язык"] = "Язык";
            language["ru"]["Сохранить"] = "Сохранить";
            language["ru"]["Версия"] = "Версия";

            language["en"]["Настройки"] = "Settings";
            language["en"]["О программе"] = "About";
            language["en"]["Логин"] = "Login";
            language["en"]["Пароль"] = "Password";
            language["en"]["Сообщение"] = "Message";
            language["en"]["Старт"] = "Start";
            language["en"]["Стоп"] = "Stop";
            language["en"]["Выполнено"] = "Executed";
            language["en"]["раз"] = "times";
            language["en"]["Статус"] = "Status";
            language["en"]["Заполните форму"] = "Fill the form";
            language["en"]["Ошибка"] = "Error";
            language["en"]["выполняется"] = "executing";
            language["en"]["ожидание"] = "waiting";
            language["en"]["отменено"] = "canceled";
            language["en"]["авторизация"] = "authorization";
            language["en"]["ошибка авторизации"] = "authorization error";
            language["en"]["Время таймера"] = "The timer";
            language["en"]["мин"] = "min";
            language["en"]["Автозапуск"] = "Auto launch";
            language["en"]["Автостарт"] = "Auto start";
            language["en"]["Хранить форму"] = "Save form";
            language["en"]["В трей"] = "To tray";
            language["en"]["Язык"] = "Language";
            language["en"]["Сохранить"] = "Save";
            language["en"]["Версия"] = "Version";

            return language[lang][word];
        }

    }
}